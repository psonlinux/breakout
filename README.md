This repo contains a simple game made in C language .

Steps for running this game on local machine :  
1)Download this folder anywhere in your computer .  
2)This game is dependent on SPL(Stanford Portable Library) library . So you must have that in pc .    

    Steps for installing spl library( https://cs50.stackexchange.com/questions/1759/running-breakout-on-mac-os-x ):  
    open a terminal and clone the spl git repo  
    user@mbp:~/$ git clone https://github.com/cs50/spl.git
    user@mbp:~/$ cd spl
    user@mbp:~/spl$ make
    this will compile the libcs.a library and the spl.jar file.
    
    You can now run
    
    user@mbp:~/spl$ sudo make install
    to install these files, which will place them in the /usr/local/lib directory.
    
    You need the spl.jar file in the same directory where you will run your breakout binary so i just copied it to my breakout directory:
    
    user@mbp:~/spl$ cd ~/breakout
    user@mbp:~/breakout$ cp /usr/local/lib/spl.jar . 
    
3)Now run make from folder where you have breakout.c .  
4)Run ./breakout  
5) Enjoy the game .

![Screenshot1](Screenshot1.png)

![Screenshot2](Screenshot2.png)

![Screenshot3](Screenshot3.png)

![Screenshot4](Screenshot4.png)