//
// breakout.c
//
// Computer Science 50
// Problem Set 4
//

// standard libraries
#define _XOPEN_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

// Stanford Portable Library
#include "gevents.h"
#include "gobjects.h"
#include "gwindow.h"

// height and width of game's window in pixels
#define HEIGHT 600
#define WIDTH 400

// number of rows of bricks
#define ROWS 5

// number of columns of bricks
#define COLS 10

// radius of ball in pixels
#define RADIUS 10

// lives
#define LIVES 3

//Max score
#define MAXSCORE  45
       
// prototypes
void initBricks(GWindow window);
GOval initBall(GWindow window);
GRect initPaddle(GWindow window);
GLabel initScoreboard(GWindow window);
void updateScoreboard(GWindow window, GLabel label, int points);
GObject detectCollision(GWindow window, GOval ball);
void movePaddle(GRect paddle);
GOval initLaserball(GWindow window,GRect paddle);
void moveLaser(GOval laserBall, GRect paddle);

int main(void)
{
    // seed pseudorandom number generator
    srand48(time(NULL));

    // instantiate window
    GWindow window = newGWindow(WIDTH, HEIGHT);

    // instantiate bricks
    initBricks(window);

    // instantiate ball, centered in middle of window
    GOval ball = initBall(window);

    // instantiate paddle, centered at bottom of window
    GRect paddle = initPaddle(window);

    // instantiate scoreboard, centered in middle of window, just above ball
    GLabel label = initScoreboard(window);

    // number of bricks initially
    int bricks = (COLS - 1) * ROWS;

    // number of lives initially
    int lives = LIVES;

    // number of points initially
    int points = 0;
    
    //rand = {1,2} Control speed of ball
    double rand =  2;
   
    //lower velocity is efficient in detecting collision correctly
    double velocity_y = 2;
    double velocity_x = 0;
    
    //laserBall from paddle
    //GOval laserBall = initLaserball(window , paddle);
    
    
    // click to start 
    waitForClick(); 
       
    // keep playing until game over
    while (lives > 0 && bricks > 0)
    {  
       //move the ball
       move(ball, velocity_x, velocity_y);
       
       //move paddle
       movePaddle(paddle);
       
       //get object collided with ball  
       GObject object = detectCollision(window, ball); 
       
       //as soon as ball collide with paddle,bricks or window border it should reverse the velocity .
       if(object != NULL){
            if(object == paddle){
                //ball can only collide from above(1 *2)
                if(velocity_x >= 0 && velocity_y >= 0){
                    velocity_x =  rand;
                    velocity_y = -rand;                
                }
                else if(velocity_x < 0 && velocity_y >= 0){
                    velocity_x = -rand;
                    velocity_y = -rand;
                }
            }
            else if( strcmp(getType(object), "GRect") == 0 ){
                //remove the collided brick.
                removeGWindow(window, object);
                
                //minus 1 from bricks count
                bricks--;
                
                //update score
                points++;
                int score = ((float)points/MAXSCORE) * 100;
                updateScoreboard(window, label, score);
                
                //in case of bricks ball can collide from below as well as above(2*2)
                if(velocity_x >= 0 && velocity_y >= 0){
                    velocity_x =  rand;
                    velocity_y = -rand;                
                }else if(velocity_x < 0 && velocity_y >= 0){
                    velocity_x = -rand;
                    velocity_y = -rand;
                }else if(velocity_x >= 0 && velocity_y < 0){
                    velocity_x = rand;
                    velocity_y =-rand;
                }else{
                    velocity_x = -rand;
                    velocity_y =rand;
                }
            }
       }
       else if( (getX(ball) + 2 * RADIUS >= WIDTH) ){
            if(velocity_y > 0){
                velocity_x = -rand;
                velocity_y = rand;
            }else{
                velocity_x = -rand;
                velocity_y = -rand;
            }
       }
       else if( getX(ball) <= 0 ){
            //if ball is going upward then keep it upward
            if(velocity_y < 0){
                velocity_x = rand;
                velocity_y = -rand;
            }
            else{
                velocity_x = rand;
                velocity_y = rand;  
            }
       }
        else if( getY(ball) <= 0 ){
             if(velocity_x >= 0 && velocity_y <= 0){
                    velocity_x =  rand;
                    velocity_y = rand;                
                }
                else{
                    velocity_x = -rand;
                    velocity_y = rand;
                }
       }
       else if( getY(ball) + 2*RADIUS >= HEIGHT ){
            //minus from lives
            lives--;
            
            //resume the bouncing
            int x = (WIDTH - RADIUS)/2;
            int y = (HEIGHT - RADIUS)/2;
            setLocation(ball, x, y);
            
            //wait for user to click
            waitForClick();
       }
       pause(5);    //slows down the ball movement 
    }

    // wait for click before exiting
    waitForClick();

    // game over
    closeGWindow(window);
    return 0;
}

/**
 * Initializes window with a grid of bricks.
 */
void initBricks(GWindow window)
{   
    int length = WIDTH/10 - 5;
    int breadth = HEIGHT/100 + 5;
    
    //gap btween rectangle
    int GAP=6;
    
    //co-ordinates of first rectangle.
    int x=15;
    int y=50;
    
    //create 5*9 =45 bricks
    for(int i=0; i<ROWS; i++){
    
        //initialize x with 15 for each new row.
        x=15;
        for(int j=1; j<COLS; j++){
            GRect rect = newGRect(x,y,length,breadth);
            setFilled(rect,true);
            setColor(rect,"GREEN");
            
            add(window,rect);
            
            
            //update x for new rectangle.
            x = x + length + GAP;
        }
        //update y for next row's rectangle.
        y = y + breadth + GAP;
    }
}

/**
 * Instantiates ball in center of window.  Returns ball.
 */
GOval initBall(GWindow window)
{
    int x = (WIDTH - 2*RADIUS)/2;
    int y = (HEIGHT - 2*RADIUS)/2;
    
    GOval ball =newGOval(x, y, 2*RADIUS, 2*RADIUS );
    setFilled(ball,true);
    add(window,ball);
    
    return ball;
}

/**
 * Instantiates paddle in bottom-middle of window.
 */
GRect initPaddle(GWindow window)
{
    int x = (WIDTH - 50) / 2;
    int y = (5 * HEIGHT) / 6;
    
    GRect rect = newGRect(x, y, WIDTH / 8, HEIGHT / 60);
    setFilled(rect, true);
    add(window, rect);
    return rect;
}

/**
 * Instantiates, configures, and returns label for scoreboard.
 */
GLabel initScoreboard(GWindow window)
{
    GLabel label = newGLabel("0");
    setFont(label, "SansSerif-30");
    add(window,label);
    return label;
}

GOval initLaserball(GWindow window,GRect paddle){
     
     //x,y of middle of paddle 
     int mpx = getX(paddle) + getWidth(paddle)/2;
     int mpy = getY(paddle);
                
     GOval laserBall = newGOval(mpx, mpy, 10, 20);
     setFilled(laserBall, true);
     add(window, laserBall);
     
     //get a refrence of laserball to move it.
     return laserBall;
}
/**
 * Updates scoreboard's label, keeping it centered in window.
 */
void updateScoreboard(GWindow window, GLabel label, int points)
{
    // update label
    char s[12];
    sprintf(s, "%i", points);
    setLabel(label, s);

    // center label in window
    double x = (getWidth(window) - getWidth(label)) / 2;
    double y = (getHeight(window) - getHeight(label)) / 2;
    setLocation(label, x, y);
}

/**
 * Detects whether ball has collided with some object in window
 * by checking the four corners of its bounding box (which are
 * outside the ball's GOval, and so the ball can't collide with
 * itself).  Returns object if so, else NULL.
 */
GObject detectCollision(GWindow window, GOval ball)
{
    // ball's location
    double x = getX(ball);
    double y = getY(ball);

    // for checking for collisions
    GObject object;

    // check for collision at ball's top-left corner
    object = getGObjectAt(window, x, y);
    if (object != NULL)
    {
        return object;
    }

    // check for collision at ball's top-right corner
    object = getGObjectAt(window, x + 2 * RADIUS, y);
    if (object != NULL)
    {
        return object;
    }

    // check for collision at ball's bottom-left corner
    object = getGObjectAt(window, x, y + 2 * RADIUS);
    if (object != NULL)
    {
        return object;
    }

    // check for collision at ball's bottom-right corner
    object = getGObjectAt(window, x + 2 * RADIUS, y + 2 * RADIUS);
    if (object != NULL)
    {
        return object;
    }

    // no collision
    return NULL;
}

/*
**Moves paddle
**/
void movePaddle(GRect paddle){
   GEvent event = getNextEvent(MOUSE_EVENT);
   if(event != NULL){
        if(getEventType(event) == MOUSE_MOVED){
            //update paddle according to cursor
            double x = 0;
            if(getX(event) <= WIDTH - getWidth(paddle)){
                x = getX(event);
            }else{
                x = WIDTH - getWidth(paddle);
            }
            double y = getY(paddle) ;
            setLocation(paddle, x, y) ;
        }            
   }
}